

build {
  sources = [
    "source.docker.alpine",
  ]

  provisioner "file" {
    source = "files/${local.packer_checksums_src}"
    destination = "/${local.packer_checksums_src}"
  }

  provisioner "shell" {
    remote_folder = "/tmp"
    inline = [
        "ls -la",
        "apk add git",
        "wget ${local.packer_download_url}",
        "sed -i '/.*linux_amd64.zip$/!d' ${local.packer_checksums_src}",
        "sha256sum -c ${local.packer_checksums_src}",
        "unzip packer_${var.packer_version}*.zip",
        "chmod +x packer",
        "mv packer /usr/local/bin/packer",
        "rm -rf packer_*",
        "rm -rf /tmp/*"
      ]
  }

  post-processors {
    post-processor "docker-tag" {
      repository  = coalesce(var.docker_repository, local.docker_repository)
      tags        = [
        join("-", [var.packer_version, "alpine3.12"]),
        join("-", [regex_replace(var.packer_version, "\\.[\\d]+$", ""), "alpine3.12"]),
        "latest"
      ]
      only        = ["docker.alpine"]
    }

    post-processor "docker-push" {}
  }
}
