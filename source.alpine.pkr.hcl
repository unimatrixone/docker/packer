

source "docker" "alpine" {
  image   = "docker:19.03.12"
  commit  = true
  changes = [
    "LABEL maintainer=${local.maintainer}",
    "LABEL version=${var.packer_version}",
    "ENTRYPOINT [\"docker-entrypoint.sh\"]",
    "CMD [\"sh\"]"
  ]
}
