# Packer Docker Image

This Packer template builds Docker images on various Linux distributions
with Packer installed.


# Prerequisites

- Docker
- Packer v1.6.1 or higher (build only).
- Push access to the configured Docker repository (build only).


# Using

- On the command-line: `docker run -it registry.gitlab.com/unimatrixone/docker/packer:latest`
- In a GitLab pipeline:

  ```
  Pack my Stuff:
    stage: provision
    image: registry.gitlab.com/unimatrixone/docker/packer:1.6.1-alpine3.12
    script:
    - packer build .
  ```


# Building

Create the file `local.pkr.hcl` with the appropriate values:

```
locals {
  docker_repository = "<replace with your Docker repository>"
}
```

Alternatively, the Docker repository may be provided through the command
line with the `-var docker_repository='"<your Docker repository>"'` parameter
(note the quoting).

Run the following command to build

```
packer build -var packer_version=<replace with Packer version> .
```


# License

MIT/BSD


# Author information

This template was created by **Cochise Ruhulessin** for the
[Unimatrix One](https://cloud.unimatrixone.io) platform.

- [Send me an email](mailto:cochise.ruhulessin@unimatrixone.io)
- [GitHub](https://github.com/cochiseruhulessin)
- [LinkedIn](https://www.linkedin.com/in/cochise-ruhulessin-0b48358a/)
