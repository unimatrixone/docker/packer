variable "docker_repository" { default=null }
variable "packer_version" { type=string }


locals {
  maintainer = "oss@unimatrixone.io"
  packer_download_url = "https://releases.hashicorp.com/packer/${var.packer_version}/packer_${var.packer_version}_linux_amd64.zip"
  packer_checksums_src = "packer_${var.packer_version}_SHA256SUMS"
}
